# translation of krunner_sessions.po to Latvian
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Maris Nartiss <maris.kde@gmail.com>, 2008.
# Viesturs Zarins <viesturs.zarins@mii.lu.lv>, 2008, 2009.
# Viesturs Zariņš <viesturs.zarins@mii.lu.lv>, 2009.
# Rūdofls Mazurs <rudolfs.mazurs@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: krunner_sessions\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-26 00:48+0000\n"
"PO-Revision-Date: 2011-07-08 12:56+0300\n"
"Last-Translator: Rūdofls Mazurs <rudolfs.mazurs@gmail.com>\n"
"Language-Team: Latvian <locale@laka.lv>\n"
"Language: lv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.1\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n != 0 ? 1 : "
"2);\n"

#: sessionrunner.cpp:28 sessionrunner.cpp:68
#, kde-format
msgctxt "log out command"
msgid "logout"
msgstr "atteikties"

#: sessionrunner.cpp:28
#, kde-format
msgid "Logs out, exiting the current desktop session"
msgstr "Atsakās, izejot no aktīvās darbvirsmas sesijas"

#: sessionrunner.cpp:29 sessionrunner.cpp:85
#, fuzzy, kde-format
#| msgctxt "shutdown computer command"
#| msgid "shutdown"
msgctxt "shut down computer command"
msgid "shut down"
msgstr "izslēgt"

#: sessionrunner.cpp:29
#, kde-format
msgid "Turns off the computer"
msgstr "Izslēdz datoru"

#: sessionrunner.cpp:33 sessionrunner.cpp:94
#, kde-format
msgctxt "lock screen command"
msgid "lock"
msgstr "slēgt"

#: sessionrunner.cpp:33
#, kde-format
msgid "Locks the current sessions and starts the screen saver"
msgstr "Slēdz aktīvo sesiju un palaiž ekrānsaudzētāju"

#: sessionrunner.cpp:36 sessionrunner.cpp:76
#, kde-format
msgctxt "restart computer command"
msgid "restart"
msgstr "pārstartēt"

#: sessionrunner.cpp:36
#, kde-format
msgid "Reboots the computer"
msgstr "Pārstartē datoru"

#: sessionrunner.cpp:37 sessionrunner.cpp:77
#, kde-format
msgctxt "restart computer command"
msgid "reboot"
msgstr "pārsākt"

#: sessionrunner.cpp:40 sessionrunner.cpp:104
#, kde-format
msgctxt "save session command"
msgid "save"
msgstr ""

#: sessionrunner.cpp:40
#, kde-format
msgid "Saves the current session for session restoration"
msgstr ""

#: sessionrunner.cpp:41 sessionrunner.cpp:105
#, fuzzy, kde-format
#| msgid "new session"
msgctxt "save session command"
msgid "save session"
msgstr "jauna sesija"

#: sessionrunner.cpp:44
#, kde-format
msgctxt "switch user command"
msgid "switch"
msgstr "pārslēgt"

#: sessionrunner.cpp:45
#, kde-format
msgctxt "switch user command"
msgid "switch :q:"
msgstr "pārslēgt :q:"

#: sessionrunner.cpp:46
#, kde-format
msgid ""
"Switches to the active session for the user :q:, or lists all active "
"sessions if :q: is not provided"
msgstr ""
"Pārslēdz uz lietotāja :q: aktīvo sesiju, vai parāda visu aktīvo sesiju "
"sarakstu, ja :q: nav norādīts"

#: sessionrunner.cpp:49 sessionrunner.cpp:144
#, kde-format
msgid "switch user"
msgstr "pārslēgt lietotāju"

#: sessionrunner.cpp:49
#, kde-format
msgid "Starts a new session as a different user"
msgstr "Sāk jaunu sesiju ar citu lietotāju"

#: sessionrunner.cpp:50 sessionrunner.cpp:144
#, kde-format
msgid "new session"
msgstr "jauna sesija"

#: sessionrunner.cpp:54
#, kde-format
msgid "Lists all sessions"
msgstr "Parāda visas sesijas"

#: sessionrunner.cpp:68
#, kde-format
msgid "log out"
msgstr "atteikties"

#: sessionrunner.cpp:70
#, kde-format
msgctxt "log out command"
msgid "Logout"
msgstr "Atteikties"

#: sessionrunner.cpp:79
#, kde-format
msgid "Restart the computer"
msgstr "Pārstartēt datoru"

#: sessionrunner.cpp:86
#, fuzzy, kde-format
#| msgctxt "shutdown computer command"
#| msgid "shutdown"
msgctxt "shut down computer command"
msgid "shutdown"
msgstr "izslēgt"

#: sessionrunner.cpp:88
#, fuzzy, kde-format
#| msgid "Shutdown the computer"
msgid "Shut down the computer"
msgstr "Izslēgt datoru"

#: sessionrunner.cpp:97
#, kde-format
msgid "Lock the screen"
msgstr "Slēgt ekārnu"

#: sessionrunner.cpp:107
#, fuzzy, kde-format
#| msgid "new session"
msgid "Save the session"
msgstr "jauna sesija"

#: sessionrunner.cpp:108
#, kde-format
msgid "Save the current session for session restoration"
msgstr ""

#: sessionrunner.cpp:129
#, kde-format
msgctxt "User sessions"
msgid "sessions"
msgstr "sesijas"

#: sessionrunner.cpp:150
#, fuzzy, kde-format
#| msgid "switch user"
msgid "Switch User"
msgstr "pārslēgt lietotāju"

#: sessionrunner.cpp:227
#, kde-format
msgid "New Session"
msgstr "Jauna sesija"

#: sessionrunner.cpp:228
#, kde-format
msgid ""
"<p>You are about to enter a new desktop session.</p><p>A login screen will "
"be displayed and the current session will be hidden.</p><p>You can switch "
"between desktop sessions using:</p><ul><li>Ctrl+Alt+F{number of session}</"
"li><li>Plasma search (type 'switch' or 'sessions')</li><li>Plasma widgets "
"(such as the application launcher)</li></ul>"
msgstr ""

#~ msgid "Warning - New Session"
#~ msgstr "Brīdinājums - jauna sesija"

#, fuzzy
#~| msgid ""
#~| "<p>You have chosen to open another desktop session.<br />The current "
#~| "session will be hidden and a new login screen will be displayed.<br />An "
#~| "F-key is assigned to each session; F%1 is usually assigned to the first "
#~| "session, F%2 to the second session and so on. You can switch between "
#~| "sessions by pressing Ctrl, Alt and the appropriate F-key at the same "
#~| "time. Additionally, the KDE Panel and Desktop menus have actions for "
#~| "switching between sessions.</p>"
#~ msgid ""
#~ "<p>You have chosen to open another desktop session.<br />The current "
#~ "session will be hidden and a new login screen will be displayed.<br />An "
#~ "F-key is assigned to each session; F%1 is usually assigned to the first "
#~ "session, F%2 to the second session and so on. You can switch between "
#~ "sessions by pressing Ctrl, Alt and the appropriate F-key at the same "
#~ "time. Additionally, the Plasma Panel and Desktop menus have actions for "
#~ "switching between sessions.</p>"
#~ msgstr ""
#~ "<p>Jūs izvēlējāties atvērt citu darbvirsmas sesiju.<br />Pašreizējā "
#~ "sesija tiks paslēpta un tiks parādīts jauns pieteikšanās logs.<br /"
#~ ">Katrai no sesijām tiek piešķirts funkcionālais taustiņš; F%1 parasti "
#~ "tiek piešķirts pirmajai sesijai, F%2 - otrajai un tā tālāk. Jūs varat "
#~ "pārslēgties starp sesijām nospiežot vienlaikus Ctrl, Alt un atbilstošās "
#~ "sesijas funkcionālo taustiņu. Papildus tam KDE panelis un darbvirsma "
#~ "piedāvā iespējas pārslēgties starp sesijuām.</p>"

#~ msgid "&Start New Session"
#~ msgstr "&Sākt jaunu sesiju"
