# Translation for kcm_users.po to Euskara/Basque (eu).
# Copyright (C) 2020-2022, This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# KDE euskaratzeko proiektuko arduraduna <xalba@ni.eus>.
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-06 00:48+0000\n"
"PO-Revision-Date: 2022-08-11 20:53+0200\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.04.3\n"

#: package/contents/ui/ChangePassword.qml:26
#: package/contents/ui/UserDetailsPage.qml:166
#, kde-format
msgid "Change Password"
msgstr "Aldatu pasahitza"

#: package/contents/ui/ChangePassword.qml:41
#, kde-format
msgid "Password"
msgstr "Pasahitza"

#: package/contents/ui/ChangePassword.qml:55
#, kde-format
msgid "Confirm password"
msgstr "Berretsi pasahitza:"

#: package/contents/ui/ChangePassword.qml:68
#: package/contents/ui/CreateUser.qml:63
#, kde-format
msgid "Passwords must match"
msgstr "Pasahitzak bat etorri behar du"

#: package/contents/ui/ChangePassword.qml:73
#, kde-format
msgid "Set Password"
msgstr "Ezarri pasahitza"

#: package/contents/ui/ChangeWalletPassword.qml:17
#, kde-format
msgid "Change Wallet Password?"
msgstr "Zorroaren pasahitza aldatu?"

#: package/contents/ui/ChangeWalletPassword.qml:26
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"Orain, saio-hasteko pasahitza aldatu duzunez, baliteke harekin bat etortzeko "
"zure KWallet lehenetsiko pasahitza aldatu nahi izatea."

#: package/contents/ui/ChangeWalletPassword.qml:31
#, kde-format
msgid "What is KWallet?"
msgstr "Zer da KWallet?"

#: package/contents/ui/ChangeWalletPassword.qml:41
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"KWallet pasahitz kudeatzaile bat da, haririk gabeko sareetarako eta "
"zifratutako beste baliabide batzuetarako pasahitzak gordetzen dituena. Bere "
"pasahitz propioarekin giltzatuta dago, saio-hasteko zure pasahitzarekiko "
"ezberdina. Bi pasahitzok bat datozenean, saio-hastean, giltzapetik "
"automatikoki askatu daiteke, KWallet-en pasahitza zuk zeuk sartu beharrik "
"izan ez dezazun."

#: package/contents/ui/ChangeWalletPassword.qml:57
#, kde-format
msgid "Change Wallet Password"
msgstr "Aldatu zorroaren pasahitza"

#: package/contents/ui/ChangeWalletPassword.qml:66
#, kde-format
msgid "Leave Unchanged"
msgstr "Utzi aldatzeke"

#: package/contents/ui/CreateUser.qml:16
#, kde-format
msgid "Create User"
msgstr "Sortu erabiltzailea"

#: package/contents/ui/CreateUser.qml:30
#: package/contents/ui/UserDetailsPage.qml:134
#, kde-format
msgid "Name:"
msgstr "Izena:"

#: package/contents/ui/CreateUser.qml:34
#: package/contents/ui/UserDetailsPage.qml:141
#, kde-format
msgid "Username:"
msgstr "Erabiltzaile-izena:"

#: package/contents/ui/CreateUser.qml:44
#: package/contents/ui/UserDetailsPage.qml:149
#, kde-format
msgid "Standard"
msgstr "Arrunta"

#: package/contents/ui/CreateUser.qml:45
#: package/contents/ui/UserDetailsPage.qml:150
#, kde-format
msgid "Administrator"
msgstr "Administratzailea"

#: package/contents/ui/CreateUser.qml:48
#: package/contents/ui/UserDetailsPage.qml:153
#, kde-format
msgid "Account type:"
msgstr "Kontu mota:"

#: package/contents/ui/CreateUser.qml:52
#, kde-format
msgid "Password:"
msgstr "Pasahitza:"

#: package/contents/ui/CreateUser.qml:56
#, kde-format
msgid "Confirm password:"
msgstr "Berretsi pasahitza::"

#: package/contents/ui/CreateUser.qml:67
#, kde-format
msgid "Create"
msgstr "Sortu"

#: package/contents/ui/FingerprintDialog.qml:57
#, kde-format
msgid "Configure Fingerprints"
msgstr "Konfiguratu hatz-markak"

#: package/contents/ui/FingerprintDialog.qml:67
#, kde-format
msgid "Clear Fingerprints"
msgstr "Garbitu hatz-markak"

#: package/contents/ui/FingerprintDialog.qml:74
#, kde-format
msgid "Add"
msgstr "Gehitu"

#: package/contents/ui/FingerprintDialog.qml:83
#: package/contents/ui/FingerprintDialog.qml:100
#, kde-format
msgid "Cancel"
msgstr "Utzi"

#: package/contents/ui/FingerprintDialog.qml:89
#, kde-format
msgid "Continue"
msgstr "Jarraitu"

#: package/contents/ui/FingerprintDialog.qml:108
#, kde-format
msgid "Done"
msgstr "Eginda"

#: package/contents/ui/FingerprintDialog.qml:131
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "Hatz-marka erroldatzea"

#: package/contents/ui/FingerprintDialog.qml:137
#, kde-format
msgctxt ""
"%1 is a type of operation (e.g. 'scan') and %2 is the name of a finger"
msgid "Please repeatedly %1 your %2 on the fingerprint sensor."
msgstr "Mesedez %1 behin eta berriz zure %2 hatz-marka sentsorean."

#: package/contents/ui/FingerprintDialog.qml:147
#, kde-format
msgid "Finger Enrolled"
msgstr "Hatza erroldatu da"

#: package/contents/ui/FingerprintDialog.qml:183
#, kde-format
msgid "Pick a finger to enroll"
msgstr "Hautatu hatz bat erroldatzeko"

#: package/contents/ui/FingerprintDialog.qml:247
#, kde-format
msgid "Re-enroll finger"
msgstr "Hatza berriz erroldatu"

#: package/contents/ui/FingerprintDialog.qml:265
#, kde-format
msgid "No fingerprints added"
msgstr "Ez da hatz-markarik gehitu"

#: package/contents/ui/main.qml:22
#, kde-format
msgid "Manage Users"
msgstr "Kudeatu erabiltzaileak"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Add New User"
msgstr "Gehitu erabiltzaile berria"

#: package/contents/ui/UserDetailsPage.qml:89
#, kde-format
msgid "Choose a picture"
msgstr "Hautatu irudi bat"

#: package/contents/ui/UserDetailsPage.qml:120
#, kde-format
msgid "Change avatar"
msgstr "Aldatu abatarra"

#: package/contents/ui/UserDetailsPage.qml:162
#, kde-format
msgid "Email address:"
msgstr "E-posta helbidea:"

#: package/contents/ui/UserDetailsPage.qml:186
#, kde-format
msgid "Delete files"
msgstr "Ezabatu fitxategiak"

#: package/contents/ui/UserDetailsPage.qml:193
#, kde-format
msgid "Keep files"
msgstr "Mantendu fitxategiak"

#: package/contents/ui/UserDetailsPage.qml:200
#, kde-format
msgid "Delete User…"
msgstr "Ezabatu erabiltzailea..."

#: package/contents/ui/UserDetailsPage.qml:211
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "Konfiguratu hatz-marka autentifikazioa..."

#: package/contents/ui/UserDetailsPage.qml:225
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"Hatz-markak erabil daitezke pasahitzen ordez pantaila giltzapetik askatzeko "
"eta behar duten aplikazio eta komando-erroko programei administratzaile "
"baimenak emateko.<nl/><nl/>Zure sisteman saio-hastea ez da onartzen oraindik."

#: package/contents/ui/UserDetailsPage.qml:235
#, kde-format
msgid "Change Avatar"
msgstr "Aldatu abatarra"

#: package/contents/ui/UserDetailsPage.qml:238
#, kde-format
msgid "It's Nothing"
msgstr "Ez da ezer"

#: package/contents/ui/UserDetailsPage.qml:239
#, kde-format
msgid "Feisty Flamingo"
msgstr "Flamenko bizia"

#: package/contents/ui/UserDetailsPage.qml:240
#, kde-format
msgid "Dragon's Fruit"
msgstr "Herensugearen fruta"

#: package/contents/ui/UserDetailsPage.qml:241
#, kde-format
msgid "Sweet Potato"
msgstr "Batata"

#: package/contents/ui/UserDetailsPage.qml:242
#, kde-format
msgid "Ambient Amber"
msgstr "Giroko anbar"

#: package/contents/ui/UserDetailsPage.qml:243
#, kde-format
msgid "Sparkle Sunbeam"
msgstr "Eguzki-izpiaren distira"

#: package/contents/ui/UserDetailsPage.qml:244
#, kde-format
msgid "Lemon-Lime"
msgstr "Lima-limoia"

#: package/contents/ui/UserDetailsPage.qml:245
#, kde-format
msgid "Verdant Charm"
msgstr "Xarma berdea"

#: package/contents/ui/UserDetailsPage.qml:246
#, kde-format
msgid "Mellow Meadow"
msgstr "Larre gozoa"

#: package/contents/ui/UserDetailsPage.qml:247
#, kde-format
msgid "Tepid Teal"
msgstr "Berde-urdinxka epela"

#: package/contents/ui/UserDetailsPage.qml:248
#, kde-format
msgid "Plasma Blue"
msgstr "Plasma urdina"

#: package/contents/ui/UserDetailsPage.qml:249
#, kde-format
msgid "Pon Purple"
msgstr "Pon purpura"

#: package/contents/ui/UserDetailsPage.qml:250
#, kde-format
msgid "Bajo Purple"
msgstr "Bajo purpura"

#: package/contents/ui/UserDetailsPage.qml:251
#, kde-format
msgid "Burnt Charcoal"
msgstr "Erretako egur-ikatza"

#: package/contents/ui/UserDetailsPage.qml:252
#, kde-format
msgid "Paper Perfection"
msgstr "Papereko perfekzioa"

#: package/contents/ui/UserDetailsPage.qml:253
#, kde-format
msgid "Cafétera Brown"
msgstr "Kafeontziko marroia"

#: package/contents/ui/UserDetailsPage.qml:254
#, kde-format
msgid "Rich Hardwood"
msgstr "Hostozabalen zur bizia"

#: package/contents/ui/UserDetailsPage.qml:305
#, kde-format
msgid "Choose File…"
msgstr "Hautatu fitxategia..."

#: src/fingerprintmodel.cpp:150 src/fingerprintmodel.cpp:257
#, kde-format
msgid "No fingerprint device found."
msgstr "Ez da hatz-marka gailurik aurkitu."

#: src/fingerprintmodel.cpp:316
#, kde-format
msgid "Retry scanning your finger."
msgstr "Saia zaitez zure hatza berriz eskaneatzen."

#: src/fingerprintmodel.cpp:318
#, kde-format
msgid "Swipe too short. Try again."
msgstr "Hatz pasaera laburregia. Berriz saiatu."

#: src/fingerprintmodel.cpp:320
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "Hatza ez dago irakurgailuan erdiratuta. Berriz saiatu."

#: src/fingerprintmodel.cpp:322
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "Kendu zure hatza irakurgailutik eta berriz saiatu."

#: src/fingerprintmodel.cpp:330
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "Hatz-marka erroldatzea huts egin du."

#: src/fingerprintmodel.cpp:333
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr ""
"Ez dago gailu honetarako lekurik, ezabatu beste hatz-marka batzuk "
"jarraitzeko."

#: src/fingerprintmodel.cpp:336
#, kde-format
msgid "The device was disconnected."
msgstr "Gailua deskonektatuta zegoen."

#: src/fingerprintmodel.cpp:341
#, kde-format
msgid "An unknown error has occurred."
msgstr "Errore ezezagun bat gertatu da."

#: src/user.cpp:267
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "Ezin izan da %1 erabiltzailea gordetzeko baimenik lortu"

#: src/user.cpp:272
#, kde-format
msgid "There was an error while saving changes"
msgstr "Errore bat gertatu da aldaketak gordetzean"

#: src/user.cpp:368
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr ""
"Irudiaren neurria aldatzea huts egin du: aldi baterako fitxategia irekitzea "
"huts egin du."

#: src/user.cpp:376
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr ""
"Irudiaren neurria aldatzea huts egin du: aldi baterako fitxategian gordetzea "
"huts egin du."

#: src/usermodel.cpp:138
#, kde-format
msgid "Your Account"
msgstr "Zure kontua"

#: src/usermodel.cpp:138
#, kde-format
msgid "Other Accounts"
msgstr "Beste kontu batzuk"

#~ msgid "Please repeatedly "
#~ msgstr "Mesedez, behin eta berriz"

#~ msgctxt "Example email address"
#~ msgid "john.doe@kde.org"
#~ msgstr "jone.agirre@kde.org"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Iñigo Salvador Azurmendi"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "xalba@euskalnet.net"

#~ msgid "Manage user accounts"
#~ msgstr "Kudeatu erabiltzaile-kontuak"

#~ msgid "Nicolas Fella"
#~ msgstr "Nicolas Fella"

#~ msgid "Carson Black"
#~ msgstr "Carson Black"

#~ msgid "Devin Lin"
#~ msgstr "Devin Lin"

#~ msgctxt "Example name"
#~ msgid "John Doe"
#~ msgstr "Jone Agirre"
