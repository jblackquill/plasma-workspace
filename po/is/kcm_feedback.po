# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# Sveinn í Felli <sv1@fellsnet.is>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-08-28 00:47+0000\n"
"PO-Revision-Date: 2022-08-26 11:45+0000\n"
"Last-Translator: Sveinn í Felli <sv1@fellsnet.is>\n"
"Language-Team: Icelandic\n"
"Language: is\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.12.3\n"

#: package/contents/ui/main.qml:34
#, kde-format
msgid ""
"User Feedback has been disabled centrally. Please contact your distributor."
msgstr ""
"Endurgjöf frá notendum hefur verið gerð óvirk miðlægt. Endilega hafðu "
"samband við þá sem sjá um dreifinguna þína."

#: package/contents/ui/main.qml:44 package/contents/ui/main.qml:77
#, kde-format
msgid "Plasma:"
msgstr "Plasma:"

#: package/contents/ui/main.qml:51
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can help KDE improve Plasma by contributing information on how you use "
"it, so we can focus on things that matter to you.<nl/><nl/>Contributing this "
"information is optional and entirely anonymous. We never collect your "
"personal data, files you use, websites you visit, or information that could "
"identify you.<nl/><nl/>You can read about <link url='https://kde.org/"
"privacypolicy-apps.php'>our privacy policy here.</link>"
msgstr ""

#: package/contents/ui/main.qml:112
#, kde-format
msgid "Plasma"
msgstr "Plasma"

#: package/contents/ui/main.qml:133
#, kde-format
msgid "The following information will be sent:"
msgstr "Eftirfarandi upplýsingar verða sendar:"

#: package/contents/ui/main.qml:178
#, kde-format
msgid "View sent data:"
msgstr "Skoða send gögn:"
